﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelevisaoCet30GUI
{
    public class Tv
    {
        #region Atributos

        private bool estado;

        private int canal;

        private string mensagem;

        private int volume;

        #endregion


        #region Construtores

        public Tv()
        {
            estado = false;
            canal = 1;
            volume = 50;
            mensagem = "Nova TV criada com sucesso";

        }

        #endregion

        #region metodos

        public int GetCanal()
        {
            return canal;
        }

        public void MudaCanal(int canal)
        {
            this.canal = canal;
        }

        public String EnviarMensagem()
        {
            return mensagem;
        }

        public void AumentoVolume(int valor)
        {
            volume += valor;
        }

        public void DiminuiVolume(int valor)
        {
            volume -= valor;
        }

        public int GetVolume()
        {
            return volume;
        }

        public bool GetEstado()
        {
            return estado;
        }

        public void LigaTv()
        {
            if(!estado)
            {
                estado = true;
                LerInfo();
                mensagem = "TV Ligada";
            }
        }

        private void LerInfo()
        {
            string ficheiro = @"tvInfo.txt";

            StreamReader sr;

            if(File.Exists(ficheiro))
            {
                sr = File.OpenText(ficheiro);

                string linha = "";

                while((linha = sr.ReadLine())!=null)
                {
                    string[] campos = new string[2];

                    campos = linha.Split(';');

                    canal = Convert.ToInt32(campos[0]);
                    volume = Convert.ToInt32(campos[1]);
                }

                sr.Close();
            }

        }

        public void DesligaTv()
        {
            if (estado)
            {
                estado = false;
                GravarInfo();
                mensagem = "TV Desligada";
            }
        }

        public void GravarInfo()
        {
            string ficheiro = @"tvInfo.txt";

            string linha = canal + ";" + volume;

            StreamWriter sw = new StreamWriter(ficheiro,false);

            if (!File.Exists(ficheiro))
            {
                sw = File.CreateText(ficheiro);
            }
                sw.WriteLine(linha);
                sw.Close(); 
            }

        #endregion
    }
}
